exports.Author = require('./author');
exports.Book = require('./book');
exports.City = require('./city');
exports.Category = require('./category');