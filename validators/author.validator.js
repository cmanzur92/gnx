const gnx = require('@simtlix/gnx');
const GNXError = gnx.GNXError; //manejo de errores de gnx.
const {Author} = require('../models/author');
const {Book} = require('../models/book');


const CantRepeatName ={ // trae el modelo de autores, 
    validate: async function(typeName, originalObject, materializedObject) {
        const AuthorFinded =
        await Author.findOne({'name': materializedObject.name}); /* FUNCIONES DE MONGOOSE. Defino el campo por el cual hacer el filtro.  materializedObject.name =>es el
         nombre que me mando el cliente, entonces lo busca en la base y va a corroborar que el id sea distinto.*/
       
        if (AuthorFinded && AuthorFinded._id != materializedObject.id) {
            throw new CantUpdateAuthorWithNameUsedError(typeName);
        }
    }};
  class CantUpdateAuthorWithNameUsedError extends GNXError {
    constructor(typeName) {
      super(typeName,'Name cant be repeated', 'CantUpdateAuthorWithNameUsedError');
    }
  };

  const CantDeleteAuthorWithBooks ={
    validate: async function(typeName, originalObject, materializedObject) {
        
        const BookFinded =
        await Book.findOne({'AuthorID': originalObject});
        
        if (BookFinded) {
            throw new CantDeleteAuthorWithBooksError(typeName);
        }
    }};
  class CantDeleteAuthorWithBooksError extends GNXError {
    constructor(typeName) {
      super(typeName,'Author have at least 1 book related', 'CantDeleteAuthorWithBooksError');
    }
  };

  const executeAuditableOnUpdating = async (objectId, modifiedObject) => {
    const promotionModel = gnx.getModel(PromotionType);
    return AuditableGraphQLObjectTypeController.onUpdating(
      objectId, modifiedObject, promotionModel
    );
  };



  module.exports ={
    CantRepeatName,
    CantDeleteAuthorWithBooks,
  };